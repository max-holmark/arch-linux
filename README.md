# Arch-Linux

## Pre-Installation

### (Optional) Set static IP address
Get the list of network interfaces
```
ip link
```

Modify `nano /etc/dhcpcd.conf` file. Example:
```
interface [interface]
static ip_address=192.168.0.10/24	
static routers=192.168.0.1
static domain_name_servers=8.8.8.8
```

Start `dhcpcd` service.
```
systemctl start dhcpcd
```

### Update the system clock
```
timedatectl set-ntp true
```

### Get the list of the disks
```
lsblk
```

### Create the disk partitions
```
cfdisk /dev/[disk]
```

### Format the partitions
- `mkfs.ext4 /dev/[partition]` for the regular partitions.
- `mkfs.fat -F32 /dev/[partition]` for the EFI partitions.
- `mkswap /dev/[partition]` format the partition for the swap.
- `swapon /dev/[partition]` enable swap on the partitions.

### Mount the partitions
1. Mount the 'root' partition `mount /dev/[partition] /mnt`
2. Create the 'boot' folded for the EFI partition `mkdir /mnt/boot`
3. Mount the 'boot' partition `mount /dev/[partition] /mnt/boot`

### Make sure to use the fastest Arch mirrors
```
reflector --country "United States" --sort rate --verbose --save /etc/pacman.d/mirrorlist
```

### Install the Arch-Linux
```
pacstrap /mnt base base-devel linux linux-firmware nano git dhcpcd grub efibootmgr bash-completion
```
Install `xf86-video-fbdev` video driver for the Hyper-V if needed
Install `open-vm-tools` and `xf86-video-vmware` video driver for the VMWare if needed (To enable: ```systemctl enable vmtoolsd.service```)


## Configure the system

### Configure FSTAB for partition auto-mouting on reboot
```
genfstab -U /mnt >> /mnt/etc/fstab
```

### "Change root" into the new system
```
arch-chroot /mnt
```

### Configure the time-zone
```
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
```

### Run hwclock to generate /etc/adjtime
```
hwclock --systohc
```

### Uncomment `en_US.UTF-8 UTF-8` and other needed locales in `/etc/locale.gen`, and generate them
```
nano /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
```

### Set host name
```
echo "[hostname]" >> /etc/hostname
```

### Set host info
```
nano /etc/hosts

127.0.0.1        localhost
::1              localhost
127.0.1.1        [hostname].localdomain [hostname]
```

### Create the admin user
```
useradd -G wheel -s /bin/bash -m -c "[FullName]" [username]
```

### Set the root and admin passwords
```
passwd [username]
```

### Give 'wheel' group root privilegies.
Open `nano /etc/sudoers` file and uncomment `%wheel ALL=(ALL) ALL` line.

### Configure the boot loader
**UEFI**
```
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=arch-linux --recheck
```

**BIOS**
```
grub-install --target=i386-pc /dev/[disk]
```

### Install intel chipset driver if needed
```
pacman -S intel-ucode
```

### Install NVidia video drivers
```
pacman -S nvidia nvidia-settings
```

### Install VirtualBox Guest Additions
```
pacman -S virtualbox-guest-utils xf86-video-vmware
```

### Configure the boot loader
```
nano /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg
```

### Optimize compile time
Navigate to [https://wiki.archlinux.org/title/makepkg#Improving_compile_times](https://wiki.archlinux.org/title/makepkg#Improving_compile_times)

### Configure SSH Server
```
pacman -S openssh
systemctl enable sshd
ssh-keygen -t ecdsa
```


### Exit new system
```
exit
```

### Reboot the system
```
reboot
```


### Set host name (live system)
```
hostnamectl hostname "[hostname]"
```


## VirtualBox virtualization issue fix on Windows 10 Host
```
1. In search box type "gpedit" then Goto  - Computer Configuration - Administrative Templates - System - Device Guard - Turn on Virtualization Based Security. Now Double click that and "Disable"

2. In search box, type Turn Windows features on or off, now uncheck Hyper-V and restart system.

3. Open Registry Editor by typing regedit in search box, now Go to HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\DeviceGuard. Add a new DWORD value named EnableVirtualizationBasedSecurity and set it to 0 to disable it.
Next Go to HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\LSA.  Add a new DWORD value named LsaCfgFlags and set it to 0 to disable it.

4. Open command prompt as a administrator and type the following commands  
bcdedit /create {0cb3b571-2f2e-4343-a879-d86a476d7215} /d "DebugTool" /application osloader

Then copy paste the rest below and press enter
bcdedit /set {0cb3b571-2f2e-4343-a879-d86a476d7215} path "\EFI\Microsoft\Boot\SecConfig.efi"
bcdedit /set {bootmgr} bootsequence {0cb3b571-2f2e-4343-a879-d86a476d7215}
bcdedit /set {0cb3b571-2f2e-4343-a879-d86a476d7215} loadoptions DISABLE-LSA-ISO,DISABLE-VBS
bcdedit /set hypervisorlaunchtype off

Now, Restart your system
```

### Add user to Virtualbox group
```
gpasswd -a $USER vboxusers
```

### Enable Virtualbox Guest Additions
```
systemctl enable --now vboxservice
```

## Install KDE

### Install the fonts
```
pacman -S ttf-dejavu ttf-freefont ttf-liberation
```

### Install KDE
```
pacman -S plasma
```

### Install extras
```
pacman -S gwenview kcolorchooser krita okular spectacle ktorrent dolphin konsole ksysguard yakuake partitionmanager ark kate kcalc 
```

### Enable system services
```
systemctl enable NetworkManager
systemctl enable sddm
```


## Basic utilities

### Default user folders
```
pacman -S xdg-user-dirs
xdg-user-dirs-update
```

### Configure the Git
```
git config --global user.name "[FullName]"
git config --global user.email [email]
git config --global core.editor [application]
```

### Install AUR helper
```
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -csi
```


## Notes

### Dotnet Core SDK VSCode bug fix
Add new exports to `.bashrc` file.
```
export DOTNET_ROOT=/opt/dotnet
export PATH=$PATH:$DOTNET_ROOT
export PATH="$PATH:/home/$USER/.dotnet/tools"
export MSBuildSDKsPath=$DOTNET_ROOT/sdk/$(${DOTNET_ROOT}/dotnet --version)/Sdks
```

### Yakuake blur transparen background fix
Install `xdotool`
```
pacman -S xdotool
```

Add the following function to `.bashrc` file.
```
if [[ $(ps --no-header -p $PPID -o comm) =~ ^yakuake$ ]]; then
    for wid in $(xdotool search --pid $PPID); do
        xprop -f _KDE_NET_WM_BLUR_BEHIND_REGION 32c -set _KDE_NET_WM_BLUR_BEHIND_REGION 0 -id $wid; done
fi
```

### Steam Torchlight 2 bug fix
```
rm ~/.local/share/Steam/steamapps/common/Torchlight\ II/lib64/libfreetype.so.6
rm ~/.local/share/Steam/steamapps/common/Torchlight\ II/lib/libfreetype.so.6
```

### CUPS add printer. Fobidden fix
```
gpasswd -a [user] lp
gpasswd -a [user] sys
```

### OpenSSL Certificate Authority

## Tutorial: https://jamielinux.com/docs/openssl-certificate-authority/create-the-root-pair.html

## Apply the root certificate:
- Copy the certificate to `/etc/ca-certificates/trust-source/anchors/` folder
- Run `trust extract-compat` command

### Ubuntu 20.04 Sound Fix for the Zephirus laptop
1. Open GRUB configuration file:
```
sudo /etc/default/grub
```
2. Find `GRUB_CMDLINE_LINUX_DEFAULT`, and add `snd_hda_intel.dmic_detect=0` to the end.
3. Apply changes:
```
sudo grub-mkconfig -o /boot/grub/grub.cfg
```
4. Reboot
